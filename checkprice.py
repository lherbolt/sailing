#!/usr/bin/env python3

# import os
import sys
import argparse
from requests import get, post
from datetime import date
from bs4 import BeautifulSoup

cl_make_name = "search-result-middle__heading"
cl_old_price = "price-box-original__old"
cl_new_price = "price-box__price ml-2"
cl_discount = "price-box-original__discount"
cl_par_name = "search-result-middle__params-name"
cl_par_val = "search-result-middle__params-value"

def config():
    parser = argparse.ArgumentParser(description='help for this script')

    parser.add_argument("--year", "-y", dest="year", action="store",
                        type=str, help="Search boat older than year.")

    parser.add_argument("--checkin", "-i", dest="checkIn", action='store',
                        type=str, help="Check-in. Valid format: yyyy-mm-dd.")

    parser.add_argument("--checkout", "-o", dest="checkOut", action='store',
                        type=str, help="Check-out. Valid format: yyyy-mm-dd.")

    parser.add_argument("--dest", "-d", dest="destinations", action='store',
                        type=str, help="Comma separated list of destinations.\
                        List destination on boataround.com")

    parser.add_argument('--currency', '-c', dest="currency", action='store',
                        default="EUR", type=str, help="Currency, possible\
                        values EUR,CZK,USD,GBP.")

    parser.add_argument('--sort', '-s', dest="sort", action='store',
                        default="priceUp", type=str, help="Sorting, Possible\
                        values [dealsFirst, reviewsDown, priceUp (default)]")

    parser.add_argument('--category', '-t', dest="category", action='store',
                        default="sailing-yacht", help="Sorting, Valid values\
                        TBD, default priceUp.")

    parser.add_argument('--crew', '-r', dest="maxSleeps", action='store',
                        type=str, help="Specify range of crew memebers.\
                        Examples: 9, 6-8, 6-, -6. ")

    parser.add_argument('--lenght', '-l', dest="boatLength", action='store',
                        type=str, help="Specify lenght of the boat in meters.\
                        Examples: 16, 16-18, 16-, -16.")

    parser.add_argument('--link', '-k', dest="link", action='store_true',
                        required=False, help="Print link to the boat.")

    arg = parser.parse_args()
    return(vars(arg))


def formatCSV(data):
    pass


def formatPretty(data):
    out = ""
    fmt1 = "{:<{mw}}{:<{nw}}{:<9}{:<10}{:<8}{:>4}{:>8}"
    fmt2 = 4*" "+"{:<{sw}}{:<{lw}}"
    fmt3 = 4*" "+"{:<10}\n"
    if opts['link']:
        fmt = fmt1+fmt2+fmt3
    else:
        fmt = fmt1+fmt2+"\n"
    mlen = nlen = slen = llen = 0
    for d in data:
        if len(d["make"]) > mlen:
            mlen = len(d["make"])
        if len(d["name"]) > nlen:
            nlen = len(d["name"])
        if len(d["Mainsail"]) > slen:
            slen = len(d["Mainsail"])
        if len(d["location"]) > llen:
            llen = len(d["location"])

    mw, nw, sw, lw = mlen+4, nlen+4, slen+4, llen
    head = fmt.format("Model", "Name", "Price", "Length", "Year", "Crew",
                      "Cabins", "Mainsail", "Location", "Link", mw=mw, nw=nw,
                      sw=sw, lw=lw)

    today = date.today()
    out += f'Price list as of: {today.strftime("%d-%m-%Y")}\n'
    out += head
    out += "="*(len(head)-1)+"\n"

    for d in data:
        boat = fmt.format(d["make"], d["name"], d["priceNew"], d["Length"],
                          d["Year"], d['People'], d['Cabins'], d['Mainsail'],
                          d['location'], d['link'], mw=mw, nw=nw, sw=sw, lw=lw)
        out += boat
    print(out, end='')


def queryBuilder(opts):
    base = 'https://www.boataround.com/search?'
    for key, val in opts.items():
        if val is not None:
            base += f'{key}={val}&'
    return base.strip('&')


def formatSearch(url, currency):
    page_iter = 1
    boats = []
    while True:
        query = url+"&page={}".format(str(page_iter))
        req = get(query, stream=True, allow_redirects=True)
        if req.status_code == 200:
            page = BeautifulSoup(req.text, 'html.parser')
            blist = BeautifulSoup(str(page.find_all(class_="search-results-list")),
                                 'html.parser')
            keys = ['link', 'make', 'name', 'priceOld', 'priceNew', 'discount',
                    'currency', 'location']
            boat = {key: None for key in keys}

            for item in blist.find_all("a"):
                boat['link'] = "https://www.boataround.com"+item["href"]
                make_name = item.find(class_=cl_make_name).get_text()
                make_name = make_name.split("|")
                boat['location'] = item.find("button").get_text().strip()
                boat['make'] = make_name[0].strip()
                boat['name'] = make_name[1].strip()
                boat['priceOld'] = item.find("span", class_ = cl_old_price) \
                    .get_text().split()[0].replace(",", ".")
                boat['discount'] = item.find("span",class_ = cl_discount) \
                    .get_text().split()[1].replace("%", "")
                boat['priceNew'] = item.find("span", class_ = cl_new_price) \
                    .get_text().split()[0].replace(",", ".")
                boat['currency'] = currency
                keys, vals = [], []
                for i in item.find_all("ul", class_ = cl_par_name):
                    for j in i.children:
                        if j.string.strip() != "":
                            keys.append(j.string)
                for i in item.find_all("ul", class_ = cl_par_val):
                    for j in i.children:
                        if j.string.strip() != "":
                            vals.append(j.string)
                res = {keys[i]: vals[i] for i in range(len(keys))}
                bt = {**boat, **res}
                boats.append(bt)
            break
    return boats


if __name__ == "__main__":
    global opts
    opts = config()
    query = queryBuilder(opts)
    boats = formatSearch(query, opts['currency'])
    formatPretty(boats)
