#Vybavení

## Na osobu
### Výstroj
* občanky/pasy
* řidičák
* prachy - CZK, PLN, (na kauci 100 EUR skipper)
  * zkusenost Mazury 2016 - 1 jidlo (pol.,ryba, pivo) cca 40 PLN vsude
  * pristavy na noc >50, na den kempy s kadibudkou 30
  * parking 80 PLN
  * benzin do lodi cca 50 PLN
* platební karty
* mapy
  * automapy
  * jezera
* spacák nebo peřina+prostěradlo
* polštářek (hodí se i na cestu)
* čelovka
* zavírák
* sluneční brejle (klapky)
* gumu na brejle

### Hygiena
* papír. kapesníky
* kartáček, zubní nit
* pasta
* ručník
* mýdlo/sprcháč
* opalovací krém
* léky

### Oblečení 
* kšiltovka/námořnická čepicka
* zmijovka
* rukavice pracovní
* plástenka - vyzkoušená je souprava kalhoty+bunda z pracovních potřeb asi za 250,-
* větrovka
* celkově obleče dle počasí (do vedra, zimy, větru, deště...)
	* boty se světlou podrážkou na palubu (dá se i naboso na vlastní nebezpečí)
	* kalhoty dlouhé
	* kraťasy
	* fusekle
	* trenky
	* trika s kratkým (pruhovaný)
	* trika s dlouhým (pruhovaný)
	* boty na pevninu
	* teplá bunda
	* mikyna
	* pokud máte dobré dětske vesty, lepší vzít své
	* holínky se světlou podrážkou dle počasí zateplené
 	(!!! opravdu si holínky vemte, zkuste jít v noci do bažiny za neodbytnými účely v něčem jiném ;) )

### Zábava
* energydrinky na cestu
* hudební nástroje, zpěvníky
* učebnice a povinná četba :)
* hudební nosiče
* rybářské náčiní
* karty, deskove hry
	* amos
	* karty
	* Panic Lab
	* bang

## Na loď
### Loď
* skipper průkaz VMP (kapitán)
* smlouva (kapitán)
* karimatky 2

### Kuchyně
* utěrky
* houbičky
* jar odbouratelný (dnes praktickz všechnz)
	* https://www.dm.cz/denkmit-pripravek-na-myti-nadobi-citronova-svezest-p4058172180378.html
* vařečka
* struhadlo
* větší prkýnko
* med, cukr, sůl, čaje, kafe
* airpress/mokkátor
* olej

### Ostatní
* sirky 3x, zapalovač, svicky, jehla, nit, panapr
* kolíčky (dřevěné)
* šňůrky 1m, delší šňůra
* ducktape
* nepotřebná audiokazeta na špiónky
* moskytiéra
* teplometná kostička (dle počasí)
* lopatka

## Při sestavování posádky
### Nastavení očekávání
* yachting není dovolená :)
* jízda a program se přizpůsobuje počasí, větru, nikoli naopak
* dobrý vítr je vzácný, tj. koupání je za bezvětří, fouká-li, jede se na plachty
* motor se používá v nouzi, přednost mají noční plavby (moře)
* bezpečnost má přednost před pohodlím a zábavou
* přeplavby mohou být dlouhé, včetně nočních plaveb (moře)
* specifika záchodů (na Mazurech na lodi nejsou, na moři ventily)

### Před vyplutím
#### Pravidla
* mravy
	* první pije Neptun (pouze ráno po vyplutí)
	* druhý kormidelník
	* třetí skipper
	* zbytek posádky
* pravidlo č. 1: kapitán má vždycky pravdu :)
* kapitán zodpovídá za bezpečnost a pokyny kapitána se plní okamžitě, bez diskuzí
* na kapitánský můstek nikdo krom kapitána nic neodkládá
* nestojí se na schůdcích do kajuty (tam vzdycky stoji Michal)

### Posádka
* zjistit, kdo ma zájem být kompetentní posádkou
* uzly 
	* lodní uzel - fendry
	* roháček, držení lan (vinšny)
		* dotahování přes roháček
		* vázání ke sloupu, 
		* liščí uzel, osma s okem, smotání lana (panenka)
* navigace lodni denik
* kde co je
* lana v zásecích - které co
* výtah hlavní plachty
* rolfok
* otěže hlavatky
* otěže kosatky
* kicking
* vinšny
* kotva, ovladač
* kača (označit, polovina celá)
* výtah kosatky (označit, nesahat)
* špionky
* naplánovat podle mapy a průvodce trasu, nespěchat při vyplutí
* kontrola motoru, chlazení, kormidlo, refy hlavní plachta, WC, startér!!! 

