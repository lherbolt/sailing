# Equipment

## Per Person
* personal id/passport
* driving license (in case of swapping in cars)
* Money - CZK, PLN, (boat deposit)
	* prices 2016 - 1 meal (soup, beer, fish ) approx 40PLN
	* marina > 50 PLN, kemping 30 PLN
	* parking 80 PLN
* credit/debit card (is better to withdraw from PL ATMs, than change money in advance)
	* maps
	* lakes 
	* driving maps (phone)
* sleeping bag or sheets, pillow
* headlight
* sunglasses
* bathroom equipment (toothbrush/paste, towel, bathsoap)
* suncream bigger factor (there is no shadow on boat)
* medicaments/drugs
* cap/hat - against sun
* working gloves
* raincoat
* windstopper
* clothes according to weather (begging of April, snowing, raining max 8°C, middle of May 25°C)
* shoes for land
* shoes for ship (light/white sole)
* wellington boot (white/light sole)
* whatever will make your stay better	
* alcohol

## Per Boat
* skipper VMP/MPVR (each skipper)
* ship agreement
* 2x sleeping pad (roll mat)
* board-games 
* phone chargers, power strip
* cleaning equipment to clean the boat

### Kitchen 	
* kitchen towels
* kitchen sponges
* dish detergent 
* wooden spoon
* grater
* bigger wooden/plastic plate
* knife sharpener, sharp knifes

### Utilities
* ducktape
* matches, lighter, candles (against mosquitos)
* ropes (1 meter , 10 meters)
* mosquito net (better to use old curtain)
* mosquito killer (battery powered)
* field showel

### Shared food
* honey, sugar
* salt, ground pepper, chilli
* butter
* soft cheese
* patte 
* tea, coffee 
* oil
* airpress, frenchpress, mokapot

# CREW
## Set Expectations
* yachting is not vacation :-)
* no electricity during day/night, only in marina
* program and directions is up to weather, wind
* no bathing as long as there is good wind
* safety first
* sailing can be long
* boats are without toilets 

## Before sailing
* who wants to be educated crew 
* knots (clove hitch! double/single 8, others)
	* tying over cleat/column
	* tying a ropes
### Terminology
#### Main sail
* mast
* main sail
* main sheet
* main halyard (lift)
* main leech 
* boom
* boomvang (kicking)

#### Jib/Genua/Genacker Sail
* rollfock
* jib sail
* jib sheet 
* jib halyard (lift)
* jib leech

#### Standing rigging	
* forestay
* backstay
* shroud
* spreader

#### Running rigging
* winch
* jammers (blockers)
* jib sheet 
* jib halyard (lift)
* main halyard (lift)
* main sheet 
* boomvang (kicking)

#### Orientation
* port (left)
* starboard (right)
* bow (front)
* stern (back)
* tiller (rudder)
* anchor
* engine control
* front propeller 

## Rules
* skipper is always right :-)
* skipper has full responsibility for ship and crew, all commands are executed immediately
* do not put anything on the bridge
* do not stand on deck stairs
## Food/meals
* one meal per day usually on ground
* breakfast and dinner or lunch on boat
* cooking for whole crew

### Dog on board
* is member of crew
* life jacket
* food, toys (floating one), place

# 2019 coordination pad
- https://beta.etherpad.org/p/maz-2019
